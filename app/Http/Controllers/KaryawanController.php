<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
	public function index()
	{
 
		return view('index');

	}

    public function show()
	{
    	// mengambil data dari tableKaryawan
		$karyawan = DB::table('karyawan')->get();

    	// mengirim dataKaryawan ke view show
		return view('show',['karyawan' => $karyawan]);

	}

	// method untuk menampilkan view form tambahKaryawan
	public function tambah()
	{

		// memanggil view tambah
		return view('tambah');

	}

	// method untuk insert data ke tableKaryawan
	public function store(Request $request)
	{
		// insert data ke tableKaryawan
		DB::table('karyawan')->insert([
			'nama_karyawan' => $request->nama,
			'no_karyawan' => $request->no,
			'no_telp_karyawan' => $request->telp,
			'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
		]);
		// alihkan halaman ke halamanKaryawan
		return redirect('/show');

	}

	// method untuk edit dataKaryawan
	public function edit($id)
	{
		// mengambil dataKaryawan berdasarkan id yang dipilih
		$karyawan = DB::table('karyawan')->where('id',$id)->get();
		// passing dataKaryawan yang didapat ke view edit.blade.php
		return view('edit',['karyawan' => $karyawan]);

	}

	// update dataKaryawan
	public function update(Request $request)
	{
		// update dataKaryawan
		DB::table('karyawan')->where('id',$request->id)->update([
			'nama_karyawan' => $request->nama,
			'no_karyawan' => $request->no,
			'no_telp_karyawan' => $request->telp,
			'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
		]);
		// alihkan halaman ke halamanKaryawan
		return redirect('/show');
	}

	// method untuk hapus dataKaryawan
	public function hapus($id)
	{
		// menghapus dataKaryawan berdasarkan id yang dipilih
		DB::table('karyawan')->where('id',$id)->delete();
		
		// alihkan halaman ke halamanKaryawan
		return redirect('/show');
	}
}
