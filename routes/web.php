<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KaryawanController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route CRUD
Route::get('/',[KaryawanController:: class, 'index']);
Route::get('/show',[KaryawanController:: class, 'show']);
Route::get('/tambah',[KaryawanController:: class, 'tambah']);
Route::post('/store',[KaryawanController:: class, 'store']);
Route::get('/edit/{id}',[KaryawanController:: class, 'edit']);
Route::post('/update',[KaryawanController:: class, 'update']);
Route::get('/hapus/{id}',[KaryawanController:: class, 'hapus']);