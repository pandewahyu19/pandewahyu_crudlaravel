<!DOCTYPE html>
<html>

<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/fd8370ec87.js" crossorigin="anonymous"></script>
</head>

<body>
    <div class="mb-4 pl-3">
        <nav class="navbar navbar-dark bg-dark fixed-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">Sistem Informasi Karyawan</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
                    <div class="offcanvas-header">
                        <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Menu</h5>
                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/tambah" style="color: black;">Tambah Data Karyawan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/show" style="color: black;">Tampil Data Karyawan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/" style="color: black;">Home</a>
                            </li>
            
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <br><br>

    <div style="margin-left:50px;margin-right:50px">
        <center>
            <div>
                <h4>Daftar Karyawan</h4>
            </div>
        </center>
<br><br>
        <table class="table table-striped mb-3 pl-4 ">
            <thead class="table-dark">
                <tr>
                    <td>
                        Id
                    </td>

                    <th>Nama</th>
                    <td>Nomer Karyawan</td>
                    <td>No Telpn</td>
                    <th>Jabatan</th>
                    <td>Divisi</td>
                    <td>Aksi</td>


                </tr>
            </thead>
            <tbody>
                @foreach($karyawan as $k)
                <tr>
                    <td>{{ $k->id }}</td>
                    <td>{{ $k->nama_karyawan }}</td>
                    <td>{{ $k->no_karyawan }}</td>
                    <td>{{ $k->no_telp_karyawan }}</td>
                    <td>{{ $k->jabatan_karyawan }}</td>
                    <td>{{ $k->divisi_karyawan }}</td>
                    <td>
                        <a href="/edit/{{ $k->id }}" class="btn btn-success">Edit</a>
                        |
                        <a href="/hapus/{{ $k->id }}" class="btn btn-danger">Hapus</a>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>

</html>