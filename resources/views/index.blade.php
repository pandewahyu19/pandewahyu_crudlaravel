<!DOCTYPE html>
<html>

<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/style.css') !!}">
    <script src="https://kit.fontawesome.com/fd8370ec87.js" crossorigin="anonymous"></script>
</head>

<body>
    <div class="mb-4 pl-3">
        <nav class="navbar navbar-dark bg-dark fixed-top">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">Sistem Informasi Karyawan</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
                    <div class="offcanvas-header">
                        <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Menu</h5>
                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/tambah" style="color: black;">Tambah Data Karyawan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/show" style="color: black;">Tampil Data Karyawan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/" style="color: black;">Home</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="banner">
        <h1>SELAMAT DATANG</h1>
        <h3>DI SISTEM INFORMASI KARYAWAN</h3>
        <div class="row">
            <div class="form-group pl-4 mb-4">
                <a href="/show" class="btn btn-dark">Tampil Data Karyawan</a>
                <a href="/tambah" class="btn btn-dark">Tambah Data Karyawan</a>
            </div>
        </div>
    </div>
    <br><br>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>